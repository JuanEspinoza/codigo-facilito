import { Navigate } from "react-router-dom";
import authHeader from "../services/auth-header";

export const ProtectedRoute = ({ children }) => {
    let user = authHeader();
    console.log("inside protected Route " + user);
    if (user == null) {
        return <Navigate to="/" />;
    }
    return children;
};
