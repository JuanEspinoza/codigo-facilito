import { Navigate } from "react-router-dom";
import authHeader from "../services/auth-header";
export const SignedRoute = ({ children }) => {
    let user = authHeader();
    console.log("inside protected signed" + user);
    if (user) {
        return <Navigate to="/adminpanel" />;
    }
    return children;
};