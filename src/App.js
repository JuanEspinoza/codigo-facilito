import React, { useState, useEffect } from "react";
import { Routes, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js";
import Login from "./components/Login";
import Register from "./components/Register";
import Home from "./components/Home";
import Profile from "./components/Profile";
import BoardUser from "./components/BoardUser";
import BoardModerator from "./components/BoardModerator";
import BoardAdmin from "./components/BoardAdmin";
import ListCreatorComponent from "./components/ListCreatorComponent"
import AddCreatorComponent from "./components/AddCreatorComponent"
import TopMenu from "./components/TopMenu";
import AdminPanel from "./components/AdminPanel";
import { ProtectedRoute } from "./routes/protected-routes";
import { SignedRoute } from "./routes/signed-routes";


const App = () => {

  return (
    <div>
      <div>
        <TopMenu></TopMenu>
      </div>
      <div className="container mt-3">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/home" element={<Home />} />
          <Route path="/login" element={
            <Login />
          } />
          <Route path="/register" element={
            <Register />
          } />
          <Route path="/profile" element={
            <ProtectedRoute>
              <Profile />
            </ProtectedRoute>
          } />
          <Route path="/adminpanel" element={
            <ProtectedRoute>
              <AdminPanel />
            </ProtectedRoute>
          }></Route>
          <Route exact path='/edit-creator/:id' element={
            <ProtectedRoute>
              <AddCreatorComponent />
            </ProtectedRoute>

          } />
          <Route exact path='/add-creator' element={
            <ProtectedRoute>
              <AddCreatorComponent />
            </ProtectedRoute>
          } />
        </Routes>
      </div>
    </div>
  );
};
export default App;