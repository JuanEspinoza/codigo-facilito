import React, { useEffect, useState } from 'react'
import CreatorService from '../services/creator.service'
import { Link } from 'react-router-dom';
import "../css/Table.css";
const ListCreatorComponent = () => {
    const [creators, setCreators] = useState();
    const [isLoading, setLoading] = useState();
    useEffect(() => {
        getAllCreators();
    }, [])

    const getAllCreators = async () => {
        setLoading(true);
        await CreatorService.getAllCreators().then((response) => {
            setCreators(response.data);
            console.log(response.data);
        }).catch(error => {
            console.log(error);
        })
        setLoading(false);
    }

    const deleteCreator = (creatorId) => {
        CreatorService.deleteCreator(creatorId).then((response) => {
            getAllCreators();
        }).catch(error => {
            console.log(error);
        });
    }



    const renderTable = () => {
        return creators.map(creator => {
            return (
                <tr key={creator.id}>
                    <td>{creator.firstname}</td>
                    <td>{creator.lastname}</td>
                    <td>{creator.email}</td>
                    <td className='long-text'>{creator.description}</td>
                    <td>
                        <Link className='btn btn-info' to={`/edit-creator/${creator.id}`}>
                            Update
                        </Link>
                        <button className='btn btn-danger' onClick={() => deleteCreator(creator.id)}>
                            Delete
                        </button>
                    </td>
                </tr>
            )
        })
    }
    const renderMensaje = () => {
        return (
            <tr>
                <td className='text-center' colSpan={5}>Loading info</td>
            </tr>
        )
    }
    return (
        <div className="container">
            <h2 className='text-center'>Creators List</h2>
            <Link to="/add-creator" className='btn btn-primary mb-2'>Add Creator</Link>
            <div className='table-responsive'>
                <table className='table table-bordered table-hover'>
                    <thead>
                        <tr class="bg-primary">
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Description</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {creators == null ? renderMensaje() : renderTable()}
                        {creators != null && creators.length == 0 ? "no recourds found" : ""}
                    </tbody>
                </table>
            </div>
        </div>

    )


}
export default ListCreatorComponent;