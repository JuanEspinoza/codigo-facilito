import React, { useEffect, useState, useRef } from 'react'
import CreatorService from '../services/creator.service';
import { Link, useNavigate, useParams } from 'react-router-dom';
import avat from "../images/avatar.png"

const AddCreatorComponent = () => {

    const [firstname, setFirstName] = useState('');
    const [lastname, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [description, setDescription] = useState('');
    const [base64photo, setbase64photo] = useState('');
    const navigate = useNavigate();
    const { id } = useParams();
    const photoUpload = useRef();

    const saveOrUpdateCreator = (e) => {
        e.preventDefault();

        const creator = { firstname, lastname, email, description, base64photo };

        if (id) {
            CreatorService.updateCreator(id, creator).then((response) => {
                navigate('/lista');
            }).catch(error => {
                console.log(error);
            });
        } else {
            CreatorService.createCreator(creator).then((reponse) => {
                navigate('/lista');
            }).catch(error => {
                console.log(error);
            });
        }
    }

    useEffect(() => {
        CreatorService.getCreatorById(id).then((response) => {
            setFirstName(response.data.firstname);
            setLastName(response.data.lastname);
            setEmail(response.data.email);
            setDescription(response.data.description);
            setbase64photo(response.data.base64photo);
        }).catch(error => {
            console.log(error);
        })
    }, []);

    const onImageClick = () => {
        console.log("inside openFileFunction");
        photoUpload.current.click();
    }
    const title = () => {
        if (id) {
            return <h2 className='text-center'>Update Creator</h2>
        } else {
            return <h2 className='text-center'>Add Creator</h2>
        }
    }
    function handleChange(e) {
        let fileInfo, baseUrl = "";
        let reader = new FileReader();

        reader.readAsDataURL(e.target.files[0]);

        reader.onload = () => {
            baseUrl = reader.result;
            setbase64photo(baseUrl);
        }
    }

    return (
        <div>
            <br></br>
            <div className='container'>
                <div className='row'>
                    <div className='car col-md-6 offset-md-3 offset-md-3'>
                        <h2 className='text-center'>{title()}</h2>
                        <div className='card-body'>
                            <form>
                                <input type="file" id='filePhoto' accept="image/png, image/jpeg" onChange={handleChange} style={{ display: 'none' }} ref={photoUpload} />
                                <img src={base64photo!=""?base64photo:avat} onClick={onImageClick} alt="Profile picture" class="img-thumbnail rounded-circle" />
                                <div className='form-group  mb-2'>
                                    <label className='form-label'>First Name:</label>
                                    <input type="text" placeholder='Enter first name'
                                        name='first-name' className='form-control' value={firstname}
                                        onChange={(e) => setFirstName(e.target.value)} />
                                </div>
                                <div className='form-group  mb-2'>
                                    <label className='form-label'>Last Name:</label>
                                    <input type="text" placeholder='Enter last name'
                                        name='last-name' className='form-control' value={lastname}
                                        onChange={(e) => setLastName(e.target.value)} />
                                </div>
                                <div className='form-group  mb-2'>
                                    <label className='form-label'>Email:</label>
                                    <input type="text" placeholder='Enter first name'
                                        name='email' className='form-control' value={email}
                                        onChange={(e) => setEmail(e.target.value)} />
                                </div>
                                <div className='form-group mb-2'>
                                    <label className='form-label'>Description:</label>
                                    <textarea placeholder='Type a short description' name='description'
                                        className='form-control' value={description}
                                        onChange={(e) => setDescription(e.target.value)}></textarea>
                                </div>
                                <button className='btn btn-success' onClick={(e) => saveOrUpdateCreator(e)}>Save</button>
                                <Link to='/adminpanel' className='btn btn-danger'>Cancel</Link>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AddCreatorComponent;