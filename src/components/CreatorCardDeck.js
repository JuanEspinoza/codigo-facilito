import React, { useEffect, useState } from 'react'
import CreatorPublicService from '../services/creator.public'

const CreatorCardDeck = () => {
    const [creators, setCreators] = useState();
    const [isLoading, setLoading] = useState();

    useEffect(() => {
        getAllCreators();
    }, [])

    const getAllCreators = async () => {
        CreatorPublicService.getAll().then((response) => {
            console.log("inside a getAll public function");
            setCreators(response.data);
            console.log(creators);
        }).catch(error => {
            console.log(error);
        })
    }

    const cardList = () => {
        console.log("Inside card list function " + creators);
    }
    return (
        <div>
            <div className="section text-center" data-aos="flip-up">
                <h1>All our creators are here :D</h1>
            </div>
            <div className='container section'>
                {creators != null && creators.lenght > 0 ? cardList : "Loading creators"}
            </div>
        </div>
    );
}
export default CreatorCardDeck;