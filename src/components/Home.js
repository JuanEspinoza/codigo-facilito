import React, { Fragment } from "react";
import CreatorCardDeck from './CreatorCardDeck'

const Home = () => {
  return (
    <Fragment>
      <div>
        <img className="img-fluid main-photo" src="https://images.pexels.com/photos/5483077/pexels-photo-5483077.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="Responsive image" />
      </div>
      <div className="section text-center" data-aos="flip-up">
        <h1>All our creators are here :D</h1>
      </div>

      <div className='container section'>
        <CreatorCardDeck></CreatorCardDeck>
      </div>

      <footer className="footer page-footer font-small bg-dark">
        <div className="container">
          <div className="row">
            <div className='col-md-4 col-xs-12'></div>
            <div className="col-md-4 col-xs-12">
              <form>
                <input type="email" className="form-control" placeholder="Enter email" />
              </form>
            </div>
            <div className='col-md-4 col-xs-12'></div>
          </div>
          <div className='row'>
            <div className='col-md-4 col-xs-12'></div>
            <div className='col-md-4 col-xs-12'>
              <p className='footer-copyright text-center text-light'>
                © 2023 Copyright:All rights reserved for Codigo Facilito React Project
              </p>
            </div>
            <div className='col-md-4 col-xs-12'></div>
          </div>
        </div>
      </footer>
    </Fragment>
  );
};

export default Home;