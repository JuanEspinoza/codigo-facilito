import React, { useState, useEffect } from "react";
import { Link, useNavigate } from 'react-router-dom';
import AuthService from "../services/auth.service";
import EventBus from "../common/EventBus";
import "../css/TopMenu.css";


const TopMenu = () => {
    const [showModeratorBoard, setShowModeratorBoard] = useState(false);
    const [showAdminBoard, setShowAdminBoard] = useState(false);
    const [currentUser, setCurrentUser] = useState(undefined);
    const [toHome, setToHome] = useState();
    const navigate = useNavigate()

    useEffect(() => {
        const user = AuthService.getCurrentUser();

        if (user) {
            setCurrentUser(user);
        }

        EventBus.on("logout", () => {
            logOut();
        });

        return () => {
            EventBus.remove("logout");
        };
    }, []);

    const logOut = () => {
        AuthService.logout();
        setShowModeratorBoard(false);
        setShowAdminBoard(false);
        setCurrentUser(undefined);
        navigate("/")
    };



    return (
        <div>
            <nav className="navbar fixed-top navbar navbar-expand-sm navbar-dark bg-dark">
                <Link to={"/"} className="navbar-brand">
                    Welcome ;)
                </Link>
                {currentUser ? (
                    <div className="navbar-nav ml-auto">

                        <div className="dropdown show">
                            <a className="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {currentUser.username}
                            </a>

                            <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a className="dropdown-item link">
                                    <Link to={"/profile"}>
                                        Profile
                                    </Link>
                                </a>
                                <a className="dropdown-item link">
                                    <Link to="/adminpanel">
                                        Admin Panel
                                    </Link>
                                </a>
                                <div className="dropdown-divider"></div>
                                <a className="dropdown-item" onClick={logOut} >
                                    Logout
                                </a>
                            </div>
                        </div>
                    </div>
                ) : (
                    <div className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <Link to={"/login"} className="nav-link">
                                Login
                            </Link>
                        </li>

                        <li className="nav-item">
                            <Link to={"/register"} className="nav-link">
                                Sign Up
                            </Link>
                        </li>
                    </div>
                )}
            </nav>
        </div>
    );
}
export default TopMenu;