import axios from "axios";

const CREATOR_BASE_REST_API_URL = "https://old-darkness-1742.fly.dev/public/coordinator";

class CreatorPublicService {
    getAll() {
        return axios.get(CREATOR_BASE_REST_API_URL);
    }
}
export default new CreatorPublicService;