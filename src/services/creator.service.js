import axios from "axios";
import authHeader from "./auth-header";

const CREATOR_BASE_REST_API_URL = "https://old-darkness-1742.fly.dev/coordinator";

class CreatorService {
    getAllCreators() {
        return axios.get(CREATOR_BASE_REST_API_URL, { headers: authHeader() });
    }

    createCreator(creator) {
        return axios.post(CREATOR_BASE_REST_API_URL, creator, { headers: authHeader() });
    }

    getCreatorById(creatorId) {
        return axios.get(CREATOR_BASE_REST_API_URL + '/' + creatorId, { headers: authHeader() });
    }

    updateCreator(creatorId, creator) {
        return axios.put(CREATOR_BASE_REST_API_URL + '/' + creatorId, creator, { headers: authHeader() });
    }

    deleteCreator(creatorId) {
        return axios.delete(CREATOR_BASE_REST_API_URL + '/' + creatorId, { headers: authHeader() });
    }
}

export default new CreatorService(); 